//
//  HistoryElementCell.swift
//  GoogleMap1
//
//  Created by Ирина Савчик on 5/14/21.
//

import UIKit

class MarkerCell: UITableViewCell {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    static func dateFormat(_ dateStr: Date) -> String {
        print("Date: \(dateStr)")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let str = dateFormatter.string(from: dateStr)
        return str
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16))
    }
    
    func setupCell(marker: Marker) {
        if let urlIcon = marker.urlIcon {
            if let url = URL(string: urlIcon) {
                IconImage.shared.downloadImage(from: url, completion: { UIImage in
                    self.iconImage.image = UIImage
                })
            }
        }
        timeLabel.text = "Request time: \(MarkerCell.dateFormat(marker.time))"
        longitudeLabel.text = "Longitude: \(String(format: "%.6f", marker.longitude))"
        latitudeLabel.text = "Latitude: \(String(format: "%.6f", marker.latitude))"
        weatherLabel.text = "\(marker.weather)"
    }
    
}
