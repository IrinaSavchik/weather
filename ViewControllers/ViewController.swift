//
//  ViewController.swift
//  GoogleMap1
//
//  Created by Ирина Савчик on 5/12/21.
//

import Foundation
import UIKit
import GoogleMaps
import CoreLocation

class ViewController: UIViewController {
    @IBOutlet weak var mapView: GMSMapView!
    
    var locationManager: CLLocationManager = CLLocationManager()
    let network = NetworkManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = .black
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        mapView.delegate = self
        
        RealmManager.shared.readObjects().forEach { (Marker) in
            print("coordinates \(Marker.latitude);\(Marker.longitude)")
        }
    }
    
    func createPin(weather: Weather?, coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        
        let pin = GMSMarker(position: coordinate)
        pin.map = mapView
        mapView.selectedMarker = pin
        if weather == nil {
            pin.snippet = "You are here"
        } else {
            guard let temperature = weather?.current?.temperature, let pressure = weather?.current?.pressure, let humidity = weather?.current?.humidity else { return }
            pin.snippet = "Temperature: \(String(format: "%.0f", temperature - 273.15))°C \nPressure: \(pressure)hPa \nHumidity: \(humidity)%"
            print("\(String(describing: pin.snippet))")
            
            let marker = Marker(time: Date(), latitude: coordinate.latitude, longitude: coordinate.longitude, weather: pin.snippet ?? "", urlIcon: weather?.current?.weatherIcon?[0].urlIcon)
            RealmManager.shared.writeObject(marker: marker)
        }
    }
    
    func updateCamera(coordinate: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition(latitude: coordinate.latitude, longitude: coordinate.longitude, zoom: mapView.camera.zoom)
        mapView.camera = camera
    }
}

extension ViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("Сlicked coordinates: \(coordinate.latitude), \(coordinate.longitude)")
        
        network.weather(longitude: coordinate.longitude, latitude: coordinate.latitude) { (Weather) in
            self.createPin(weather: Weather, coordinate: coordinate)
        } failure: { (str) in
            print("Error while determining weather: \(str)")
        }
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        createPin(weather: nil, coordinate: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
        updateCamera(coordinate: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
    }
}

extension UIViewController {
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }

        return instantiateFromNib()
    }
}
