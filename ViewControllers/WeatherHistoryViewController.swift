//
//  CurrentWeatherViewController.swift
//  GoogleMap1
//
//  Created by Ирина Савчик on 5/13/21.
//

import UIKit

class WeatherHistoryViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var data: [Marker] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCell()
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        data = RealmManager.shared.readObjects()
        tableView.reloadData()
    }
    
    @IBAction func deleteAllDataActionButton(_ sender: Any) {
        RealmManager.shared.removeAllData()
        data = RealmManager.shared.readObjects()
        tableView.reloadData()
    }
    
    func setupCell() {
        let nib = UINib(nibName: String(describing: MarkerCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: MarkerCell.self))
    }
}

extension WeatherHistoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension WeatherHistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MarkerCell.self), for: indexPath)
        guard let markerCell = cell as? MarkerCell else { return cell }
        
        let item = data[indexPath.row]
        markerCell.setupCell(marker: item)
        
        return markerCell
    }
}



