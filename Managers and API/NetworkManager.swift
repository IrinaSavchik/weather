//
//  NetworkManager.swift
//  GoogleMap1
//
//  Created by Ирина Савчик on 5/13/21.
//

import Foundation
import Moya
import Moya_ObjectMapper

final class NetworkManager {
    private let provider = MoyaProvider<NetworkService>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
    
    private init() {
    }
    
    static let shared = NetworkManager()
    
    func weather(longitude: Double, latitude: Double, completion: @escaping (Weather) -> Void, failure: @escaping (String) -> Void)  {
        provider.request(.getWeather(longitude: longitude, latitude: latitude)) { (result) in
            switch result {
            case let .success(response):
                guard let mappedResponse = try? response.mapObject(Weather.self) else {
                    failure("Unknown")
                    return
                }
                completion(mappedResponse)
            case .failure(let error):
                failure(error.errorDescription ?? "Unknown")
            }
        }
    }
}



