//
//  RealmManager.swift
//  GoogleMap1
//
//  Created by Ирина Савчик on 5/14/21.
//

import Foundation
import RealmSwift

class RealmManager {
    static let shared = RealmManager()
    let realm = try! Realm()
    
    private init() { }
    
    func writeObject(marker: Marker) {
        try! realm.write {
            realm.add(marker)
        }
    }
    
    func removeAllData() {
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func readObjects() -> [Marker] {
        return Array(realm.objects(Marker.self))
    }
}
