//
//  BackenAPI.swift
//  GoogleMap1
//
//  Created by Ирина Савчик on 5/13/21.
//

import Foundation
import Moya
import Alamofire

enum NetworkService {
    case getWeather(longitude: Double, latitude: Double)
}

class ApiKey {
    static var apiKey = "31a137bf04ae3cb41664cc3c9b1f6bf5"
}

extension NetworkService: TargetType {
    var baseURL: URL {
        return URL(string: "https://api.openweathermap.org/")!
    }
    
    var path: String {
        switch self {
        case .getWeather:
            return "data/2.5/onecall"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getWeather:
            return .get
        }
    }
    
    var sampleData: Data {
        Data()
    }
    
    var task: Task {
        switch self {
        default:
            guard let params = parameters else {
                return .requestPlain
            }
            return .requestParameters(parameters: params, encoding: parameterEncoding)
        }
    }
    
    var headers: [String : String]? {
        switch self {
        default:
            return [:]
        }
    }
    
    var parameters: [String: Any]? {
        var params = [String: Any]()
        
        switch self {
        case .getWeather(let longitude, let latitude):
            params["lat"] = latitude
            params["lon"] = longitude
            params["appid"] = ApiKey.apiKey
        }
        return params
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .getWeather:
            return URLEncoding.queryString
        }
    }
}
