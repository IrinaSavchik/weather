//
//  Coordinates.swift
//  GoogleMap1
//
//  Created by Ирина Савчик on 5/13/21.
//

import Foundation

class Credentials: Encodable {
    var longitude = 0.0
    var latitude = 0.0
    
    init(longitude: Double, latitude: Double) {
        self.longitude = longitude
        self.latitude = latitude
    }
    
    func asParams() -> [String: Any] {
        var params = [String: Any]()
        params["lon"] = self.longitude
        params["lat"] = self.latitude
        return params
    }
}
