//
//  Marker.swift
//  GoogleMap1
//
//  Created by Ирина Савчик on 5/13/21.
//

import Foundation
import RealmSwift
import GoogleMaps

class Marker: Object {
    @objc dynamic var time: Date = Date()
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude: Double = 0.0
    @objc dynamic var weather: String = ""
    @objc dynamic var urlIcon: String?
    
    convenience init(time: Date, latitude: Double, longitude: Double, weather: String, urlIcon: String?) {
        self.init()
        self.time = time
        self.latitude = latitude
        self.longitude = longitude
        self.weather = weather
        self.urlIcon = urlIcon
    }
}

