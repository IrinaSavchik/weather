//
//  Weather.swift
//  GoogleMap1
//
//  Created by Ирина Савчик on 5/13/21.
//

import Foundation
import ObjectMapper

class Weather : Mappable {
    var current: Current?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        current    <- map["current"]
    }
}

class Current : Mappable {
    var temperature: Float?
    var pressure: Int?
    var humidity: Int?
    var weatherIcon: [WeatherIcon]?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        temperature     <- map["temp"]
        pressure        <- map["pressure"]
        humidity        <- map["humidity"]
        weatherIcon     <- map["weather"]
    }
}

class WeatherIcon : Mappable {
    var icon: String?
    var urlIcon: String {
        return "https://openweathermap.org/img/wn/\(icon ?? "10d")@2x.png"
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        icon       <- map["icon"]
    }
}
