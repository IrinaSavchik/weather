//
//  WeatherIcon.swift
//  GoogleMap1
//
//  Created by Ирина Савчик on 20.05.21.
//
//
import Foundation
import UIKit

class IconImage {
    static let shared = IconImage()
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL, completion: @escaping (UIImage) -> ()) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                completion(UIImage(data: data) ?? UIImage())
            }
        }
    }
}
